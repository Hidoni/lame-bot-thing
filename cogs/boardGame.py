import asyncio
import datetime
import json
import os
import random
import aiofiles
import aiohttp
import discord
from PIL import Image
from opencv_manipulator import generate_pfp_in_circle
from discord.ext import commands

BOT_CHANNEL_ID = 0  # Enter channel here


def write_json(board):
    with open("players.json", 'w') as output:
        full_json = {}
        for player in players:
            data = player.for_json()
            full_json[data[0]] = data[1]
        output.write(json.dumps(full_json))
    with open("board.json", 'w') as output:
        full_json = {}
        for space in board:
            data = space.to_json()
            full_json[data[0]] = [*data[1:]]
        output.write(json.dumps(full_json))


class BoardSpace:
    def __init__(self, name, investors, owner, identifier):
        self.name = name
        self.investors = investors
        self.owner = owner
        self.id = identifier

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name.lower() == other.lower()

    def find_owner(self):
        highest_investor = [None, 0]
        for investor in self.investors.items():
            if investor[1] > highest_investor[1]:
                highest_investor[0] = investor[0]
                highest_investor[1] = investor[1]
        return highest_investor[0]

    def to_json(self):
        new_owner = self.owner.id if self.owner else 0
        new_investors = {}
        for investor in self.investors.items():
            new_investors[investor[0].id] = investor[1]
        return self.id, new_investors, new_owner, self.name

    def not_property(self):
        return self.name in ["Go", "Community Chest", "Chance", "Income Tax", "Furry Hell", "Go To Furry Hell", "Free Parking", "Luxury Tax"]

    def is_line(self):
        return "Railroad" in self.name

    def is_water_works(self):
        return self.name == "Aquarium"

    def is_electric_company(self):
        return self.name == "Charging Station"

    async def handle_user_landing(self, channel, player):
        """
        Gets called when a user lands on the tile, Reacting as necessary
        """
        if self.not_property():
            if self.name == "Go":
                await channel.send(f"{player.user.display_name} lands on {self.name}, Earning an additional 20 coins!")
                player.coins += 50
            elif self.name == "Community Chest" or self.name == "Chance":
                await self.handle_chest_chance(channel, player)
            elif self.name == "Income Tax":
                await channel.send(f"{player.user.display_name} lands on {self.name}, losing 50 coins.")
                player.coins -= 50
            elif self.name == "Go To Furry Hell":
                await channel.send(f"{player.user.display_name} has been arrested and sent to Furry Hell!")
                player.board_position = 10
                player.jailed = True
                player.time_in_jail = 3
            elif self.name == "Luxury Tax":
                await channel.send(f"{self.name} costs {player.user.display_name} 15 coins.")
                player.coins -= 15
            elif self.name == "Free Parking":
                await channel.send(f"{player.user.display_name} lands on Free Parking and nothing happens.")
            elif self.name == "Furry Hell":
                await channel.send(f"{player.user.display_name} is visiting {self.name}")
        elif self.is_line():
            await channel.send("This is a Railroad Line, You can type $ride to ride to a different (random!) Railroad Line for 10 coins!")
        elif self.is_water_works():
            await channel.send("This is Hidoni's Aquarium.\nIf you own either SimStart Charging Station or Hidoni's Aquarium you get 4 points, if you own both of them, you get 16!")
        elif self.is_electric_company():
            await channel.send("This is the SimStart Charging Station.\nIf you own either Charging Station orAquarium you get 4 points, if you own both of them, you get 16!")
        elif self.owner is None:
            await channel.send(f"Nobody owns {self.name}, you can be the first to do so by typing $invest \\*number* and your chosen amount!")
        elif player == self.owner.user:
            await channel.send(f"{player.user.display_name} already owns {self.name}.")
        else:
            try:
                await channel.send(f"{self.owner.user.display_name} owns {self.name}, You can take {self.name} from them by investing {self.investors[self.owner] - self.investors[player] + 1} coins")
            except KeyError:
                await channel.send(f"{self.owner.user.display_name} owns {self.name}, You can take {self.name} from them by investing {self.investors[self.owner] + 1} coins")

    async def handle_chest_chance(self, channel, player):
        if self.name == "Chance":
            event_list = ["get_out", "goto_St. Zabivaka Place", "Railroad", "get_50", "back_3", "jail", "lose_25", "lose_15", "goto_Popufur Catwalk", "get_150", "get_100", "lose_100"]  # Chance Events
        else:
            event_list = ["get_out", "goto_Go", "get_200", "lose_50", "jail", "get_100", "get_20", "lose_50", "get_25", "get_10", "goto_Hidoni's Aquarium", "goto_SimStart Charging Station"]  # Chest Events
        event = random.choice(event_list)
        if event == "get_out":
            if player.jailed:
                player.jailed = False
                player.time_in_jail = 0
                await channel.send(f"{player.user.display_name} receives a Get Out Of Hell Free Card and leaves Furry Hell immediately!")
            else:
                if not player.has_get_out_card:
                    player.has_get_out_card = True
                    await channel.send(f"{player.user.display_name} receives a Get Out Of Hell Free Card and keeps it for next time he's in Furry Hell!")
                else:
                    await self.handle_chest_chance(channel, player)  # Just call the function dick until we get something that isn't get_out
        elif event.startswith("goto_"):
            space = event.split('_')[1]
            new_position = ["Go", "Sergal Avenue", "Community Chest", "Sea Salt Avenue", "Income Tax",
                            "Bad Dragon Railroad", "Lewd Avenue", "Chance", "Pepper Coyote Avenue", "Werewolf Avenue", "Furry Hell",
                            "St. Zabivaka Place", "Charging Station", "Majira Avenue", "Kyell Gold Avenue",
                            "SMH Railroad", "St. Likulau Place", "Community Chest", "Bulge Avenue",
                            "Gay Avenue", "Free Parking", "Red Rocket Avenue", "Chance", "r/furry_irl Avenue",
                            "Tony the Tiger Avenue", "N & L Railroad", "Corneria Avenue", "Sparkledog Avenue", "Aquarium",
                            "Zootopia Gardens", "Go To Furry Hell", "Pawcific Avenue", "Nos Hyena Avenue", "Community Chest",
                            "Wilde Avenue", "Shorts Railroad", "Chance", "MSG Marketplace", "Luxury Tax", "Popufur Catwalk"].index(space)
            await channel.send(f"{player.user.display_name}'s card sends him to {space}!")
            if player.board_position > new_position:
                await channel.send(f"{player.user.display_name} passes Go and gets 50 coins!")
                player.coins += 50
            player.board_position = new_position
        elif event == "Railroad":
            if player.board_position < 5 or player.board_position > 35:
                player.board_position = 5
            elif 5 < player.board_position < 15:
                player.board_position = 15
            elif 15 < player.board_position < 25:
                player.board_position = 25
            else:
                player.board_position = 35
            await channel.send(f"{player.user.display_name} moves forward to the nearest railroad!")
        elif event.startswith("get_"):
            money = int(event.split('_')[1])
            player.coins += money
            await channel.send(f"{player.user.display_name} gets {money} coins for free!")
        elif event.startswith("back_"):
            spaces = int(event.split('_')[1])
            player.board_position -= spaces
            await channel.send(f"{player.user.display_name} gets sent back {spaces} spaces!")
        elif event == "jail":
            player.board_position = 10
            player.jailed = True
            player.time_in_jail = 3
            await channel.send(f"{player.user.display_name} has been sent to Furry Hell!")
        elif event.startswith("lose"):
            money = int(event.split('_')[1])
            await channel.send(f"{player.user.display_name} loses {money} coins!")

    async def handle_user_investing(self, ctx, player, investment):
        owner = self.find_owner()
        if not self.not_property() and not self.is_line():
            if player in self.investors.keys():
                self.investors[player] += investment
            else:
                self.investors[player] = investment
            if self.owner is None or self.investors[player] > self.investors[owner]:
                await ctx.send(f"{player.user.display_name} has invested {investment} coins and now owns {self.name}!")
                self.owner = player
            elif player == self.owner.user:
                await ctx.send(f"{self.owner.user.display_name} invests {investment} and keeps his ownership of {self.name}.")
            else:
                await ctx.send(f"{player.user.display_name} has invested {investment}, but {self.owner.user.display_name} still owns {self.name}")
        else:
            await ctx.send("This is not a property you can invest in!")


class BoardPlayer:
    def __init__(self, user, last_message, coins, message_count, has_move, board_position, jailed, time_in_jail, has_card):
        self.user = user
        self.id = user.id
        self.last_message = last_message
        self.coins = coins
        self.message_count = message_count
        self.has_move = has_move
        self.board_position = board_position
        self.jailed = jailed
        self.time_in_jail = time_in_jail
        self.processing = False
        self.has_get_out_card = has_card

    def __repr__(self):
        return str(self.user.id)

    def __str__(self):
        return self.user.display_name

    def __eq__(self, other):
        return self.user == other

    def __hash__(self):
        return self.id

    @classmethod
    def new_player(cls, user):
        return cls(user, datetime.datetime.now().strftime("%d/%m/%Y, %H:%M"), 100, 0, False, 0, False, 0, False)

    async def avatar_to_file(self):
        url = self.user.avatar_url_as(format='png')
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                async with aiofiles.open(f'avatars/{self.id}.png', mode='wb') as file:
                    await file.write(await response.read())

    def for_json(self):
        return self.id, {"lastMessage": self.last_message, "Coins": self.coins, "message_count": self.message_count, "hasMove": self.has_move, "boardPosition": self.board_position, "inJail": self.jailed, "turnsInJail": self.time_in_jail, "hasGetOufOfJailCard": self.has_get_out_card}

    def handle_user_message(self):
        current_date = datetime.datetime.now()
        previous_date = datetime.datetime.strptime(self.last_message, "%d/%m/%Y, %H:%M")
        if current_date > previous_date + datetime.timedelta(0, 60):
            self.coins += 1
            self.message_count += 1
            self.last_message = current_date.strftime("%d/%m/%Y, %H:%M")
            if self.message_count >= 3:
                self.has_move = True
                self.message_count = 0


def find_user(user):
    for player in players:
        if player == user:
            return player


class BoardCog:
    def __init__(self, bot):
        self.bot = bot
        if os.path.isfile("board.json"):
            self.board = []
            full_json = json.load(open("board.json"))
            for space in full_json.items():
                fixed_investors = {}
                for investor in space[1][0].items():
                    fixed_investors[find_user(bot.get_user(int(investor[0])))] = investor[1]
                fixed_owner = find_user(bot.get_user(space[1][1]))
                self.board.append(BoardSpace(space[1][2], fixed_investors, fixed_owner, space[0]))
        else:
            temp_board = ["Go", "Sergal Avenue", "Community Chest", "Sea Salt Avenue", "Income Tax",
                          "Bad Dragon Railroad", "Lewd Avenue", "Chance", "Pepper Coyote Avenue", "Werewolf Avenue", "Furry Hell",
                                "St. Zabivaka Place", "SimStart Charging Station", "Majira Avenue", "Kyell Gold Avenue",
                                "SMH Railroad", "St. Likulau Place", "Community Chest", "Bulge Avenue",
                                "Gay Avenue", "Free Parking", "Red Rocket Avenue", "Chance", "r/furry_irl Avenue",
                                "Tony the Tiger Avenue", "N & L Railroad", "Corneria Avenue", "Sparkledog Avenue", "Hidoni's Aquarium",
                                "Zootopia Gardens", "Go To Furry Hell", "Pawcific Avenue", "Nos Hyena Avenue", "Community Chest",
                                "Wilde Avenue", "Shorts Railroad", "Chance", "MSG Marketplace", "Luxury Tax", "Popufur Catwalk"]
            self.board = [BoardSpace(temp_board[x], {}, None, x) for x in range(40)]

    async def roll(self, player):
        channel = self.bot.get_channel(BOT_CHANNEL_ID)
        first_roll = random.randint(1, 6)
        second_roll = random.randint(1, 6)
        await channel.send(f"{player.user.mention} is rolling the dice!")
        await channel.trigger_typing()
        await asyncio.sleep(2)
        await channel.send(f"{player.user.display_name} rolls the first die and gets {first_roll}!")
        await channel.trigger_typing()
        await asyncio.sleep(2)
        await channel.send(f"{player.user.display_name} rolls the second die and gets {second_roll}!")
        previous_position = player.board_position
        if not player.jailed:
            player.board_position = (player.board_position + first_roll + second_roll) % 40
            if previous_position > player.board_position:
                await channel.send(f"{player.user.display_name} has completed a lap around the board, earning 50 coins")
                player.coins += 50
            await channel.send(f"{player.user.display_name} moves {first_roll + second_roll} spaces and lands on {self.board[player.board_position]}!")
            await channel.trigger_typing()
            await self.board[player.board_position].handle_user_landing(channel, player)
        else:
            leaving_jail = False
            if first_roll == second_roll:
                leaving_jail = True
                await channel.send(f"{player.user.display_name} rolls doubles and gets to leave Furry Hell!")
            else:
                player.time_in_jail -= 1
                if player.time_in_jail == 0:
                    await channel.send(f"{player.user.display_name} has spent his three turns in Furry Hell and is now free!")
                    leaving_jail = True
                else:
                    await channel.send(f"{player.user.display_name} has {player.time_in_jail} turns left in Furry Hell")
            if leaving_jail:
                player.time_in_jail = 0
                player.jailed = 0
                player.board_position = (player.board_position + first_roll + second_roll) % 40
                await channel.send(f"{player.user.display_name} moves {first_roll + second_roll} spaces and lands on {self.board[player.board_position]}!")
                await channel.trigger_typing()
                await self.board[player.board_position].handle_user_landing(channel, player)
        write_json(self.board)

    async def check_member(self, member):
        player = find_user(member)
        if not player:
            player = BoardPlayer.new_player(member)
            players.append(player)
        if not player.processing:
            player.processing = True
            player.handle_user_message()
            if player.has_move:
                player.has_move = False
                await self.roll(player)
            write_json(self.board)
            player.processing = False

    @commands.command()
    async def stats(self, ctx):
        player = find_user(ctx.author)
        if not player:
            player = BoardPlayer.new_player(ctx.author)
            players.append(player)
        embed_list = []
        embed_index = 0
        embed_list.append(discord.Embed(title=f"Stats of {ctx.author.display_name}", colour=discord.Colour.dark_red()))
        embed_list[embed_index].add_field(name="Your Coins:", value=f"{player.coins} coins", inline=False)
        for space in self.board:
            if player in space.investors.keys():
                embed_list[embed_index].add_field(name=f"Invested amount in {space}:", value=f"{space.investors[player]} coins", inline=False)
                if len(embed_list[embed_index].fields) == 25:
                    embed_index += 1
                    embed_list.append(discord.Embed(title=f"Stats of {ctx.author.display_name} (Continued)", colour=discord.Colour.dark_red()))
        for embed in embed_list:
            await player.user.send(embed=embed)

    @commands.command()
    async def invest(self, ctx, investment: int):
        player = find_user(ctx.author)
        if not player:
            player = BoardPlayer.new_player(ctx.author)
            players.append(player)
        if player.coins >= investment:
            await self.board[player.board_position].handle_user_investing(ctx, player, investment)
            player.coins -= investment
        else:
            await ctx.send(f"Sorry {player.user.display_name}, but you don't have enough coins!")

    @commands.command()
    async def ride(self, ctx):
        player = find_user(ctx.author)
        if not player:
            player = BoardPlayer.new_player(ctx.author)
            players.append(player)
        if self.board[player.board_position].is_line():
            if player.coins >= 10:
                lines = [5, 15, 25, 35]
                lines.remove(player.board_position)
                new_position = random.choice(lines)
                await ctx.send(f"{player.user.display_name} boards a train at the {self.board[player.board_position]} for 10 coins, and arrives at {self.board[new_position]}!")
                player.board_position = new_position
                player.coins -= 10
            else:
                await ctx.send(f"Sorry {player.user.display_name}, but you don't have enough coins!")
        else:
            await ctx.send(f"Sorry {player.user.display_name}, but you're not at a Railroad Line!")

    @commands.command(aliases=['board'])
    async def _board(self, ctx, *tile: str):
        embed_list = []
        embed_index = 0
        if not tile:
            embed_list.append(discord.Embed(title="All Spaces", colour=discord.Colour.dark_red()))
            for space in self.board:
                embed_list[embed_index].add_field(name=space, value="Owned by: " + space.owner.user.display_name if space.owner is not None else "Nobody", inline=False)
                if len(embed_list[embed_index].fields) == 25:
                    embed_index += 1
                    embed_list.append(discord.Embed(title="All Spaces (continued)", colour=discord.Colour.dark_red()))
        else:
            tile = ' '.join(tile)
            if tile in self.board:
                for space in self.board:
                    if tile == space:
                        break
                embed_list.append(discord.Embed(title=f"All investors of {space.name}", colour=discord.Colour.dark_red()))
                for investor in space.investors.keys():
                    embed_list[embed_index].add_field(name=investor.user.display_name, value=f"{space.investors[investor]} coins", inline=False)
                    if len(embed_list[embed_index].fields) == 25:
                        embed_index += 1
                        embed_list.append(discord.Embed(title=f"All investors of {tile} (Continued)", colour=discord.Colour.dark_red()))
            else:
                await ctx.send("That's not a space on the board!")
        for embed in embed_list:
            await ctx.author.send(embed=embed)

    @commands.command()
    async def position(self, ctx):
        player = find_user(ctx.author)
        if not player:
            player = BoardPlayer.new_player(ctx.author)
            players.append(player)
        await player.avatar_to_file()
        avatar_original = Image.open(f'avatars/{player.id}.png').convert('RGBA')
        avatar_original.save(f'avatars/{player.id}.png')
        for color in avatar_original.getcolors(avatar_original.height * avatar_original.width):
            pixel = color[1]
            if (pixel[0] > 225 and pixel[1] > 225 and pixel[2] > 225) or pixel[3] != 255:
                continue
            break
        pixel = list(pixel)
        pixel[0] = abs(255 - pixel[0])
        pixel[1] = abs(255 - pixel[1])
        pixel[2] = abs(255 - pixel[2])
        generate_pfp_in_circle(player.id, pixel)
        avatar = Image.open(f'avatars/{player.id}_processed.png')
        board = Image.open("monopoly.png")
        if player.board_position > 30:
            avatar = avatar.rotate(90)
        elif player.board_position > 20:
            avatar = avatar.rotate(180)
        elif player.board_position > 10:
            avatar = avatar.rotate(270)
        if player.board_position in [0, 10, 20, 30] and not player.jailed:
            avatar = avatar.resize((round(avatar.height * 3.45), round(avatar.width * 3.45)), Image.ANTIALIAS)
        elif player.jailed:
            avatar = avatar.resize((360, 360), Image.ANTIALIAS)
        else:
            resize_values = {1: (481, 344), 2: (442, 344), 3: (462, 344), 4: (468, 344), 5: (468, 344), 6: (477, 344), 7: (469, 344), 8: (469, 344), 9: (474, 344), 11: (355, 469), 12: (355, 455), 13: (355, 474), 14: (355, 468), 15: (355, 468), 16: (355, 475), 17: (355, 469), 18: (355, 469), 19: (355, 472), 21: (475, 355), 22: (457, 355), 23: (475, 355), 24: (468, 355), 25: (470, 355), 26: (475, 355), 27: (468, 355), 28: (468, 355), 29: (453, 355), 31: (368, 473), 32: (368, 467), 33: (368, 470), 34: (368, 477), 35: (368, 469), 36: (368, 468), 37: (368, 475), 38: (368, 456), 39: (368, 468)}
            resize_value = resize_values[player.board_position]
            avatar = avatar.resize(resize_value, Image.ANTIALIAS)
        board_positions = {0: (5200, 5200), 1: (4695, 5656), 2: (4235, 5656), 3: (3755, 5656), 4: (3269, 5656), 5: (2782, 5656), 6: (2288, 5656), 7: (1802, 5656), 8: (1316, 5656), 9: (824, 5656), 10: (0, 5200), 11: (0, 4717), 12: (0, 4244), 13: (0, 3750), 14: (0, 3264), 15: (0, 2777), 16: (0, 2284), 17: (0, 1796), 18: (0, 1310), 19: (0, 820), 20: (0, 0), 21: (824, 0), 22: (1316, 0), 23: (1790, 0), 24: (2283, 0), 25: (2769, 0), 26: (3257, 0), 27: (3750, 0), 28: (4237, 0), 29: (4724, 0), 30: (5194, 0), 31: (5633, 820), 32: (5633, 1309), 33: (5632, 1796), 34: (5632, 2283), 35: (5631, 2775), 36: (5631, 3264), 37: (5631, 3750), 38: (5631, 4244), 39: (5631, 4717)}
        if player.board_position == 10:
            if player.jailed:
                avatar = avatar.rotate(-37)
                board.paste(avatar, (356, 5301), avatar)
            else:
                board.paste(avatar, board_positions[player.board_position], avatar)
        else:
            board.paste(avatar, board_positions[player.board_position], avatar)
        board.save(f'avatars/{player.id}_board.png')
        await ctx.send(file=discord.File(f'avatars/{player.id}_board.png'))

    @commands.command()
    async def debug(self, ctx):
        await self.roll(find_user(ctx.author))


def setup(bot):
    global players
    players = []
    if os.path.isfile("players.json"):
        for user in json.load(open("players.json")).items():
            values = [x[1] for x in user[1].items()]
            players.append(BoardPlayer(bot.get_user(int(user[0])), *values))
    bot.add_cog(BoardCog(bot))
