import cv2


def generate_pfp_in_circle(id, circle_color):
    original = cv2.imread(f'avatars/{id}.png', cv2.IMREAD_UNCHANGED)
    original = cv2.resize(original, (236, 236))
    original = cv2.circle(original, (118, 118), 118, tuple(circle_color), 15)
    rows = original.shape[0]
    columns = original.shape[1]
    for i in range(columns):
        first_black = False
        final_black = False
        for j in range(rows):
            pixel = original[i, j].tolist()
            if not first_black:
                if pixel == circle_color:
                    first_black = True
                else:
                    original[i, j] = [0, 0, 0, 0]
            elif first_black and not final_black:
                if pixel == circle_color:
                    try:
                        if original[i, j + 1].tolist() != circle_color and j >= 118:
                            final_black = True
                    except IndexError:
                        pass
            elif first_black and final_black:
                original[i, j] = (0, 0, 0, 0)
    cv2.imwrite(f"avatars/{id}_processed.png", original)
