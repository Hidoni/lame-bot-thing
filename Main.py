import discord
from discord.ext import commands
import logging

logging.basicConfig(format="%(asctime)s %(levelname)s %(message)s", level="DEBUG", handlers=[logging.FileHandler("bot.log", encoding="utf-8", mode='a')])
console = logging.StreamHandler()
console.setLevel("INFO")
console.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s"))
logging.getLogger("").addHandler(console)

client = commands.Bot(command_prefix='$', case_insensitive=True, pm_help=True)
GUILD_ID = 0  # Enter server ID here


@client.event
async def on_ready():
    logging.info("Logged in as {0.user.name}, ID Is: {0.user.id}".format(client))
    try:
        client.load_extension("cogs.boardGame")
        logging.info("Loaded board cog successfully")
    except Exception as e:
        logging.info(f"Failed to load cog because of the following error: {e}")
    global board_funcs
    board_funcs = client.cogs['BoardCog']
    await client.change_presence(activity=discord.Game("Always Watching 👀"))


@client.event
async def on_message(message):
    if not message.author.bot and message.guild.id == GUILD_ID:
        await board_funcs.check_member(message.author)
        await client.process_commands(message)

client.run("NDU0NzkwNDU1NjU0MDg4NzE2.DfykNw.hAWnf2Kim9mVKBSIyTvsSz_nK9Q")
